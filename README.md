---
title: 
date: 2023-01-29 20:14:23
tags:

---

## 获取lol英雄数据


```go
GetLolHero() (api.T)
// 返回所有 数据
```

```go
GetLolHeroForName(name string) (api.Hero)
// 输入名字 返回 指定英雄数据
```