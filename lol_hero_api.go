package goApi

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

var (
	/// ---=-=-=-=---- api 英雄列表
	url = "http://game.gtimg.cn/images/lol/act/img/js/heroList/hero_list.js"
	// 英雄详情
	xiangqin = "http://game.gtimg.cn/images/lol/act/img/js/hero/%s.js"
	Data     T
)

func GetLolHeroForName(name string) Hero {
	GetLolHero()
	var h Hero
	for i, hero := range Data.Hero {
		if hero.Name == name {
			fmt.Println("name -->", name)
			h = Data.Hero[i]
			break
		} else {
			continue
		}
	}
	return h
}

// GetLolHero 返回数据 Data
func GetLolHero() T {
	data := httpget(url)
	if err := json.Unmarshal(data, &Data); err != nil {
	}
	fmt.Println(len(Data.Hero), "位英雄-")
	//fmt.Println(string(data))
	for num, i := range Data.Hero {
		//i --> {1 黑暗之女 Annie 安妮 [mage] 0 2 3 10 6 https://game.gtimg.cn/images/lol/act/img/vo/choose/1.ogg https://game.gtimg.cn/images/lol/act/img/vo/ban/1.ogg 0 0 无改动 4800 2000   安妮,黑暗之女,火女zhinv,huonv,an,hazn,hn 0b95894e-0df2-470e-b282-6c5f5cf41955   []}
		//fmt.Println("i -->", i)
		setHeroPng(Data, num, i.HeroId)
		getyuantu(num)
	}
	return Data
}

func getyuantu(heroid int) YuanTu {
	var tmpList YuanTu
	var pf = Data.Hero[heroid].Zl.Skins
	for i := 0; i < len(Data.Hero[i].HeroId); i++ {
		tmpList.Name = pf[i].Name
		if pf[i].MainImg == "" {
			tmpList.Url = pf[i].ChromaImg
		} else {
			tmpList.Url = pf[i].MainImg
		}
		Data.Hero[heroid].Yt = append(Data.Hero[heroid].Yt, tmpList)
	}
	return YuanTu{}
}

type YuanTu struct {
	Name string
	Url  string
}
type T struct {
	Hero     []Hero `json:"hero"`
	Version  string `json:"version"`
	FileName string `json:"fileName"`
	FileTime string `json:"fileTime"`
}
type Hero struct {
	HeroId              string   `json:"heroId"`
	Name                string   `json:"name"`
	Alias               string   `json:"alias"`
	Title               string   `json:"title"`
	Roles               []string `json:"roles"`
	IsWeekFree          string   `json:"isWeekFree"`
	Attack              string   `json:"attack"`
	Defense             string   `json:"defense"`
	Magic               string   `json:"magic"`
	Difficulty          string   `json:"difficulty"`
	SelectAudio         string   `json:"selectAudio"`
	BanAudio            string   `json:"banAudio"`
	IsARAMweekfree      string   `json:"isARAMweekfree"`
	Ispermanentweekfree string   `json:"ispermanentweekfree"`
	ChangeLabel         string   `json:"changeLabel"`
	GoldPrice           string   `json:"goldPrice"`
	CouponPrice         string   `json:"couponPrice"`
	Camp                string   `json:"camp"`
	CampId              string   `json:"campId"`
	Keywords            string   `json:"keywords"`
	InstanceId          string   `json:"instance_id"`
	Yt                  []YuanTu
	Zl                  X
}
type X struct {
	Hero struct {
		HeroId               string        `json:"heroId"`
		Name                 string        `json:"name"`
		Alias                string        `json:"alias"`
		Title                string        `json:"title"`
		Roles                []string      `json:"roles"`
		Camp                 string        `json:"camp"`
		CampId               string        `json:"campId"`
		Attack               string        `json:"attack"`
		Defense              string        `json:"defense"`
		Magic                string        `json:"magic"`
		Difficulty           string        `json:"difficulty"`
		Hp                   string        `json:"hp"`
		Hpperlevel           string        `json:"hpperlevel"`
		Mp                   string        `json:"mp"`
		Mpperlevel           string        `json:"mpperlevel"`
		Movespeed            string        `json:"movespeed"`
		Armor                string        `json:"armor"`
		Armorperlevel        string        `json:"armorperlevel"`
		Spellblock           string        `json:"spellblock"`
		Spellblockperlevel   string        `json:"spellblockperlevel"`
		Attackrange          string        `json:"attackrange"`
		Hpregen              string        `json:"hpregen"`
		Hpregenperlevel      string        `json:"hpregenperlevel"`
		Mpregen              string        `json:"mpregen"`
		Mpregenperlevel      string        `json:"mpregenperlevel"`
		Crit                 string        `json:"crit"`
		Critperlevel         string        `json:"critperlevel"`
		Attackdamage         string        `json:"attackdamage"`
		Attackdamageperlevel string        `json:"attackdamageperlevel"`
		Attackspeed          string        `json:"attackspeed"`
		Attackspeedperlevel  string        `json:"attackspeedperlevel"`
		HeroVideoPath        string        `json:"heroVideoPath"`
		IsWeekFree           string        `json:"isWeekFree"`
		DamageType           string        `json:"damageType"`
		Style                string        `json:"style"`
		DifficultyL          string        `json:"difficultyL"`
		Damage               string        `json:"damage"`
		Durability           string        `json:"durability"`
		CrowdControl         string        `json:"crowdControl"`
		Mobility             string        `json:"mobility"`
		Utility              string        `json:"utility"`
		SelectAudio          string        `json:"selectAudio"`
		BanAudio             string        `json:"banAudio"`
		ChangeLabel          string        `json:"changeLabel"`
		GoldPrice            string        `json:"goldPrice"`
		CouponPrice          string        `json:"couponPrice"`
		Keywords             string        `json:"keywords"`
		Introduce            string        `json:"introduce"`
		PalmHeroHeadImg      string        `json:"palmHeroHeadImg"`
		Relations            []interface{} `json:"relations"`
	} `json:"hero"`
	Skins []struct {
		SkinId          string `json:"skinId"`
		HeroId          string `json:"heroId"`
		HeroName        string `json:"heroName"`
		HeroTitle       string `json:"heroTitle"`
		Name            string `json:"name"`
		Chromas         string `json:"chromas"`
		ChromasBelongId string `json:"chromasBelongId"`
		IsBase          string `json:"isBase"`
		EmblemsName     string `json:"emblemsName"`
		MainImg         string `json:"mainImg"`
		IconImg         string `json:"iconImg"`
		LoadingImg      string `json:"loadingImg"`
		VideoImg        string `json:"videoImg"`
		SourceImg       string `json:"sourceImg"`
		VedioPath       string `json:"vedioPath"`
		SuitType        string `json:"suitType"`
		PublishTime     string `json:"publishTime"`
		ChromaImg       string `json:"chromaImg"`
	} `json:"skins"`
	Spells []struct {
		HeroId             string      `json:"heroId"`
		SpellKey           string      `json:"spellKey"`
		Name               string      `json:"name"`
		Description        string      `json:"description"`
		AbilityIconPath    string      `json:"abilityIconPath"`
		AbilityVideoPath   string      `json:"abilityVideoPath"`
		DynamicDescription string      `json:"dynamicDescription"`
		Cost               interface{} `json:"cost"`
		Costburn           string      `json:"costburn"`
		Cooldown           interface{} `json:"cooldown"`
		Cooldownburn       string      `json:"cooldownburn"`
		Range              interface{} `json:"range"`
	} `json:"spells"`
	Version  string `json:"version"`
	FileName string `json:"fileName"`
	FileTime string `json:"fileTime"`
}

// setHeroPng 添加英雄图片数据
func setHeroPng(x T, datai int, HeroId string) {
	var xq X
	data := httpget(fmt.Sprintf(xiangqin, HeroId))
	if err := json.Unmarshal(data, &xq); err != nil {
	}
	x.Hero[datai].Zl = xq

	//var tmpList YuanTu
	//var pf = xq.Skins
	//for i := 0; i < len(pf); i++ {
	//	tmpList.Name = xq.Skins[i].Name
	//	if pf[i].MainImg == "" {
	//		tmpList.Url = pf[i].ChromaImg
	//	} else {
	//		tmpList.Url = pf[i].MainImg
	//	}
	//
	//	x.Hero[datai].Yt = append(x.Hero[datai].Yt, tmpList)
	//}
	//
}

// get   html / json
func httpget(u string) (data []byte) {
	// 得到一个客户端
	client := &http.Client{}
	request, _ := http.NewRequest("GET", u, nil)
	request.Header.Set("User-Agent", "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Mobile Safari/537.36")
	//request.Header.Add("Cookie", "test_cookie=CheckForPermission; expires=Tue, 30-Aug-2022 01:04:32 GMT; path=/; domain=.doubleclick.net; Secure; HttpOnly; SameSite=none")
	// 客户端发送请求，并且获取一个响应
	res, err := client.Do(request)
	if err != nil {
		log.Println("Error: ", err)
		return []byte("错误--！")
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
		}
	}(res.Body) // 关闭
	data, err = io.ReadAll(res.Body)
	if err != nil {
		fmt.Println("数据错误：", err)
		return []byte("错误--！")
	}
	return
}
